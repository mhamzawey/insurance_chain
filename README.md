**Installation of development environment:**

1- cd ~/fabric-dev-servers

2- ./startFabric.sh

3- ./createPeerAdminCard.sh

**NOTE:**
You can start and stop your runtime using ``~/fabric-dev-servers/stopFabric.sh``, and start it again with ``~/fabric-dev-servers/startFabric.sh``.
At the end of your development session, you run ``~/fabric-dev-servers/stopFabric.sh`` and then ``~/fabric-dev-servers/teardownFabric.sh``. **P.S** that if you've run the teardown script, the next time you start the runtime, you'll need to create a new PeerAdmin card just like you did on first time startup.


**To Presist the Data in CouchDB**:

Modify `fabric-dev-tools/fabcir-scripts/hlfv11/composer/docker-compose.yml` and add 
```
volumes:
        - /etc/couchdb:/opt/couchdb/data
```
to couchdb container


**Creating your first project:**

Create the skeleton:

```yo hyperledger-composer:businessnetwork```

1- Enter ``insurance_chain`` for the network name, and desired information for description, author name, and author email.

2- Select Apache-2.0 as the license.

3- Select org.example.mynetwork as the namespace.

4- Select No when asked whether to generate an empty network or not.



**Generate a business network archive:**

From the insurance_chain directory, execute this command:
```composer archive create -t dir -n .```

**Deploying the business network:**

1- After creating the .bna file, the business network can be deployed to the instance of Hyperledger Fabric. Normally, information from the Fabric administrator is required to create a PeerAdmin identity, with privileges to install chaincode to the peer as well as start chaincode on the composerchannel channel. However, as part of the development environment installation, a PeerAdmin identity has been created already.

2- After the business network has been installed, the network can be started. For best practice, a new identity should be created to administer the business network after deployment. This identity is referred to as a network admin. For the development env. We should follow those steps:

3- The composer network install command requires a PeerAdmin business network card (in this case one has been created and imported in advance), and the the file path of the .bna which defines the business network.

```composer network install --card PeerAdmin@hlfv1 --archiveFile insurance_chain@0.0.1.bna```

**To start the business network, run the following command:**

```composer network start --networkName insurance_chain --networkVersion 0.0.1 --networkAdmin admin --networkAdminEnrollSecret adminpw --card PeerAdmin@hlfv1 --file networkadmin.card```

The composer network start command requires a business network card, as well as the name of the admin identity for the business network, the name and version of the business network and the name of the file to be created ready to import as a business network card.

**To import the network administrator identity as a usable business network card, run the following command:**

```composer card import --file networkadmin.card```

The composer card import command requires the filename specified in composer network start to create a card.

To check that the business network has been deployed successfully, run the following command to ping the network:

```composer network ping --card admin@insurance_chain```



**To Upgrade your Business Network**

1- Update the version property in `package.json` file.

2- From your business network directory run the `composer archive create` command: `composer archive create -t dir -n .`

3- `composer network install -a insurance_chain@0.0.6.bna -c PeerAdmin@hlfv1`

4- To upgrade the network: `composer network upgrade -c PeerAdmin@hlfv1 -n insurance_chain -V 0.0.9`


Generating a REST server
To create the REST API, navigate to the insurance_chain directory and run the following command:
composer-rest-server



**To do a POST request**

*To create a new Checkpoint*

```
{
  "$class": "org.example.insurancechain.CheckPoint",
  "checkPointId": "reception",
  "insuranceProvider":"resource:org.example.insurancechain.InsuranceProvider#insuranceProviderName:AXA" ,
  "description": "reception check point"
}
```
`insuranceProvider` is a foreign key it should be referenced by this syntax: `resource:org.example.insurancechain.<assetName>#<PrimaryKey>:<PrimaryKeyValue>`

*To create a new Transaction*

```
{
  "$class": "org.example.insurancechain.TapTransaction",
  "checkPoint":"resource:org.example.insurancechain.CheckPoint#checkPointId:reception" ,
  "Patient": "resource:org.example.insurancechain.Patient#patientID:0123456",
  "description": "Reception Transaction",
  "timestamp": "2018-07-30T15:00:08.651Z"
}
```

**For Equivilence Filters**

`{"where":{"insuranceProvider":"resource:org.example.insurancechain.InsuranceProvider#insuranceProviderName:AXA"}}`

**For neq Filters**

`{"where":{"insuranceProvider":{"neq":"resource:org.example.insurancechain.InsuranceProvider#insuranceProviderName:AXA"}}}`

**LoopBack Filters**

``{"where:{"field1":{"op":Value1}}}``



**To write Smart Contract**:
`https://hyperledger.github.io/composer/latest/reference/js_scripts`

**IF using Yarn**

`export PATH="$HOME/.yarn-global/node_modules/.bin:$PATH"`


**To Enable Authentication**:

*Github*:
`https://hyperledger.github.io/composer/latest/integrating/enabling-rest-authentication`

*Google*:
`https://hyperledger.github.io/composer/latest/tutorials/google_oauth2_rest`

1- `docker run -d --name mongo --network composer_default -p 27017:27017 mongo`
2- `source envvars.txt`
3- Install & Start Network

**When running a dokcer container as REST SERVER**:

```sed -e 's/localhost:7051/peer0.org1.example.com:7051/' -e 's/localhost:7053/peer0.org1.example.com:7053/' -e 's/localhost:7054/ca.org1.example.com:7054/'  -e 's/localhost:7050/orderer.example.com:7050/'  < $HOME/.composer/cards/restadmin@insurance_chain/connection.json  > /tmp/connection.json && cp -p /tmp/connection.json $HOME/.composer/cards/restadmin@insurance_chain/ ```


```sed -e 's/localhost:7051/peer0.org1.example.com:7051/' -e 's/localhost:7053/peer0.org1.example.com:7053/' -e 's/localhost:7054/ca.org1.example.com:7054/'  -e 's/localhost:7050/orderer.example.com:7050/'  < $HOME/.composer/cards/mhamza@insurance_chain/connection.json  > /tmp/connection.json && cp -p /tmp/connection.json $HOME/.composer/cards/mhamza@insurance_chain/```





**AWS**

`https://docs.aws.amazon.com/blockchain-templates/latest/developerguide/blockchain-templates-hyperledger.html`

`https://medium.com/coinmonks/get-started-with-blockchain-using-the-aws-hyperledger-fabric-template-an-unofficial-guide-551bc46af710`

`https://medium.com/@david.richard.holtz/blockchain-application-demo-b98321ec318c`

