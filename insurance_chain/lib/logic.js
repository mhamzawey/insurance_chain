/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';


function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}
let _id = getRandomInt(1000)
/**
 * Set up the run
 * @param {org.example.insurancechain.claim.CreateSubmittedClaim} createSubmittedClaim - the setUp transaction instance
 * @transaction
 */
async function createSubmittedClaim(createSubmittedClaim) {
    const NS = 'org.example.insurancechain.claim';
    const theFactory = getFactory();
    // Create a submitted dental claim
    console.log('Create a submitted dental claim - id = getRandomInt()');
    let submittedDentalClaim = theFactory.newResource(NS, 'SubmittedDentalClaim', _id.toString());
    const val = submittedDentalClaim.getClassDeclaration().getDecorator('description').getArguments()[0];
    console.log('SubmittedDentalClaims decorator: ' + val);
    submittedDentalClaim.dentalClaimType = 'ENCOUNTER';
    // Predetermination is omitted
    submittedDentalClaim.planName = 'Some primary dental plan';
    // Create a Plan Address
    let aPlanAddress = theFactory.newConcept(NS, 'Address');
    aPlanAddress.addr1 = '123 Street';
    aPlanAddress.city = 'Cairo';
    submittedDentalClaim.planName = 'Some primary dental plan';
    submittedDentalClaim.planAddress = aPlanAddress;
    // Other coverage omitted 
    // Policyholder/subscriber information
    // Name
    let aMemberName = theFactory.newConcept(NS, 'HumanName');
    aMemberName.firstName = 'Mohamed';
    aMemberName.lastName = 'Hamza';
    submittedDentalClaim.memberName = aMemberName;
    let aMemberClaimAddress = theFactory.newConcept(NS, 'Address');
    aMemberClaimAddress.addr1 = '123 Street';
    aMemberClaimAddress.city = 'Cairo';
    submittedDentalClaim.memberAddress = aMemberClaimAddress;
    submittedDentalClaim.memberDateOfBirth = new Date('1990/05/2');
    submittedDentalClaim.memberGender = 'MALE';
    submittedDentalClaim.memberId = '100';
    submittedDentalClaim.planOrGroupNumber = '123';
    submittedDentalClaim.employerName = 'Insurance Chain';
    // Patient information
    submittedDentalClaim.patientsRelationshipToPolicyHolder = 'SELF';
    if (submittedDentalClaim.patientsRelationshipToPolicyHolder === 'SELF') {
        submittedDentalClaim.patientName = submittedDentalClaim.memberName;
        submittedDentalClaim.patientAddress = submittedDentalClaim.memberAddress;
        submittedDentalClaim.patientDateOfBirth = submittedDentalClaim.memberDateOfBirth;
        submittedDentalClaim.patientGender = submittedDentalClaim.memberGender;
    }
    submittedDentalClaim.dentistPatientId = '123';
    // Service lines
    submittedDentalClaim.submittedDentalServiceLines = [];
    theSubmittedDentalServiceLines = []; // Objects to save
    let totalFee = 0;
    let toothNumber = 10;
    //Three Service lines 3 Back Teeth operation (Oral Cavity)
    for (let i = 0; i < 3; i++) {
        let aSubmittedDentalServiceLine = theFactory.newResource(NS, 'SubmittedDentalServiceLine', 'SL' + (_id+i).toString());
        theSubmittedDentalServiceLines.push(aSubmittedDentalServiceLine);
        let aSubmittedDentalServiceLineReln = theFactory.newRelationship(NS, 'SubmittedDentalServiceLine', 'SL' + (_id+i).toString());
        aSubmittedDentalServiceLine.dentalServiceLineId = (_id+i).toString();
        aSubmittedDentalServiceLine.lineNumber = i;
        aSubmittedDentalServiceLine.procedureDate = new Date(Date.now() - 2 * 86400000); // 2 days ago
        aSubmittedDentalServiceLine.areaOfOralCavity = 'Back';
        aSubmittedDentalServiceLine.toothSystem = 'A';
        toothNumber += i;
        aSubmittedDentalServiceLine.toothNumberOrLetter = toothNumber + '';
        aSubmittedDentalServiceLine.toothSurface = 'POST';
        aSubmittedDentalServiceLine.procedureCode = '120XXXX';
        aSubmittedDentalServiceLine.diagPointer = toothNumber + '';
        aSubmittedDentalServiceLine.quantity = 1;
        aSubmittedDentalServiceLine.description = 'Description';
        aSubmittedDentalServiceLine.fee = 800 * 2 * i
        totalFee += aSubmittedDentalServiceLine.fee;
        submittedDentalClaim.submittedDentalServiceLines.push(aSubmittedDentalServiceLineReln);
    }
    submittedDentalClaim.totalFees = totalFee / 100;
    submittedDentalClaim.amount = 1800 * totalFee
    
    submittedDentalClaim.patientGuardianSignature = true;
    submittedDentalClaim.subscriberSignature = true;
    submittedDentalClaim.placeOfTreatmentCode = 11; // clinic
    // Billing provider --> in this case the clinic is both the billing provider and the treatment provider
    let aBillingProviderName = theFactory.newConcept(NS, 'HumanName');
    aBillingProviderName.firstName = 'Clinic';
    aBillingProviderName.lastName = 'Contact';
    console.log('Billing provider name: ' + aBillingProviderName);
    submittedDentalClaim.billingProviderName = aBillingProviderName;
    // Billing provider address
    let aBillingProviderAddress = theFactory.newConcept(NS, 'Address');
    aBillingProviderAddress.addr1 = '123 Street';
    aBillingProviderAddress.city = 'Cairo';
    submittedDentalClaim.billingProviderAddress = aBillingProviderAddress;
    submittedDentalClaim.billingProviderNpi = '1234567890'; //National Provider identifier -> 10 digits
    submittedDentalClaim.billingProviderLicense = 'CA 42'; //Provider license 
    submittedDentalClaim.billingProviderTin = '23-789'; //Tax Identification Number 
    submittedDentalClaim.billingProviderPhoneNumber = '022559678';
    // Treating provider
    let aTreatingProviderName = theFactory.newConcept(NS, 'HumanName');
    aTreatingProviderName.firstName = 'Clinic';
    aTreatingProviderName.lastName = 'Contact';
    console.log('Treating provider name: ' + aTreatingProviderName);
    submittedDentalClaim.treatingProviderName = aTreatingProviderName;
    // Treating provider details
    submittedDentalClaim.treatingProviderNpi = '1234567890';
    submittedDentalClaim.treatingProviderLicense = 'CA 42';
    submittedDentalClaim.treatingProviderPhoneNumber = '022559678';
    // Treating provider address
    let aTreatingProviderAddress = theFactory.newConcept(NS, 'Address');
    aTreatingProviderAddress.addr1 = '123 Street';
    aTreatingProviderAddress.city = 'Cairo';
    submittedDentalClaim.treatingProviderAddress = aTreatingProviderAddress;
    // Write it to the ledger
    console.log('Submitted claim: ' + JSON.stringify(getSerializer().toJSON(submittedDentalClaim)));
    const submittedDentalServiceLineRegistry = await getAssetRegistry(NS + '.SubmittedDentalServiceLine');
    await submittedDentalServiceLineRegistry.addAll(theSubmittedDentalServiceLines);
    const submittedClaimRegistry = await getAssetRegistry(NS + '.SubmittedDentalClaim');
    console.log('Adding submitted dental claim: ' + submittedDentalClaim);
    await submittedClaimRegistry.addAll([submittedDentalClaim]);
}

/**
 * Adjudicate a claim
 * @param {org.example.insurancechain.claim.AdjudicateDentalClaim} adjudicateDentalClaim - the adjudicateDentalClaim transaction instance
 * @transaction
 */
async function adjudicateDentalClaim(adjudicateDentalClaim) {
    /*
    // TODO This is a 10 step process ...
    1. Field edits
    2. Membership eligibility
    3. Provider eligibility
    4. Duplicate claim checking
    5. Benefit and referral eligibility
    6. Pricing resolution
    7. Benefit resolution
    8. Coordination of benefits (COB) resolution
    9. End of line resolution
    10. File updates
    */
    // Create an adjudicated claim that references the submitted claim - lame PoC
    const NS = 'org.example.insurancechain.claim';
    const theFactory = getFactory();
    let theAdjudicatedDentalClaim = theFactory.newResource(NS, 'AdjudicatedDentalClaim', _id.toString());
    theAdjudicatedDentalClaim.claimStatus = 'HELD';
    theAdjudicatedDentalClaim.dentalClaimType = 'ENCOUNTER';
    theAdjudicatedDentalClaim.amount = adjudicateDentalClaim.submittedDentalClaim.amount; // TODO this needs to be a big decimal!
    theAdjudicatedDentalClaim.submittedDentalClaim = theFactory.newRelationship(NS, 'SubmittedDentalClaim', adjudicateDentalClaim.submittedDentalClaim.$identifier);
    console.log('Add the new adjudicated dental claim: ' + theAdjudicatedDentalClaim);
    const adjudicatedDentalClaimAssetRegistry = await getAssetRegistry(NS + '.AdjudicatedDentalClaim');
    console.log('Add the new adjudicated dental claim: ' + theAdjudicatedDentalClaim);
    await adjudicatedDentalClaimAssetRegistry.add(theAdjudicatedDentalClaim);
    console.log('Send the dental claim event for ac id: ' + theAdjudicatedDentalClaim.claimId);
    let event = theFactory.newEvent(NS, 'StartAdjudicationEvent');
    event.adjudicatedDentalClaim = theFactory.newRelationship(NS, 'AdjudicatedDentalClaim', theAdjudicatedDentalClaim.claimId);
    event.submittedDentalClaim = theFactory.newRelationship(NS, 'SubmittedDentalClaim', adjudicateDentalClaim.submittedDentalClaim.$identifier);
    emit(event);
}

/**
 * Pay a claim
 * @param {org.example.insurancechain.claim.PayDentalClaim} payDentalClaim - pay the adjudicated claim
 * @transaction
 */
async function payDentalClaim(payDentalClaim) {
    const theAdjudicatedDentalClaim = payDentalClaim.adjudicatedDentalClaim;
    const theAdjudicatedDentalClaimId = theAdjudicatedDentalClaim.getIdentifier();
    console.log('The adjudicated dental claimId: ' + theAdjudicatedDentalClaimId);
    if (payDentalClaim.adjudicatedDentalClaim.dentalClaimType != 'ENCOUNTER') {
        throw new Error('Only encounters can be paid');
    }
    const NS = 'org.example.insurancechain.claim';
    const theFactory = getFactory();

    let thePayment = theFactory.newResource(NS, 'Payment', _id.toString());
    thePayment.amount = theAdjudicatedDentalClaim.amount; // Just pay the amount
    thePayment.billingProvider = theFactory.newRelationship(NS, 'BillingProvider', theAdjudicatedDentalClaim.submittedDentalClaim.billingProviderNpi);
    thePayment.adjudicatedDentalClaim = theFactory.newRelationship(NS, 'AdjudicatedDentalClaim', theAdjudicatedDentalClaimId);
    //console.log('The payment: ' + JSON.stringify(getSerializer().toJSON(thePayment)));
    const adjudicatedDentalClaimAssetRegistry = await getAssetRegistry(NS + '.AdjudicatedDentalClaim');
    theAdjudicatedDentalClaim.claimStatus = 'PAID';
    await adjudicatedDentalClaimAssetRegistry.update(theAdjudicatedDentalClaim);
    console.log('Payment asset registry promise');
    const paymentAssetRegistry = await getAssetRegistry(NS + '.Payment');
    await paymentAssetRegistry.add(thePayment);
    console.log('Send the claim paid event');
    let event = theFactory.newEvent(NS, 'PayDentalClaimEvent');
    console.log('Adjudicated dental claim id: ' + theAdjudicatedDentalClaim.dentalClaimId);
    event.adjudicatedDentalClaim = theFactory.newRelationship(NS, 'AdjudicatedDentalClaim', theAdjudicatedDentalClaim.$identifier);
    event.payment = theFactory.newRelationship(NS, 'Payment', thePayment.$identifier);
    emit(event);
}

