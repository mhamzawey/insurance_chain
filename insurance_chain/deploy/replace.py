import os
from tempfile import mkstemp
from shutil import move
from os import fdopen, remove
import sys

def get_content(filename):
        with file(filename) as f:
                s = f.read()
        return s

def replace(file_path, pattern, subst):
    #Create temp file
    fh, abs_path = mkstemp()
    with fdopen(fh,'w') as new_file:
        with open(file_path) as old_file:
            for line in old_file:
                new_file.write(line.replace(pattern, subst))
    #Remove original file
    remove(file_path)
    #Move new file
    move(abs_path, file_path)

orderer_cert = get_content("./fabric-samples/first-network/crypto-config/ordererOrganizations/example.com/orderers/orderer.example.com/tls/ca.crt").replace('\n','\\n')
org1_cert = get_content("./fabric-samples/first-network/crypto-config/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt").replace('\n','\\n')
org2_cert = get_content("./fabric-samples/first-network/crypto-config/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt").replace('\n','\\n')



def main():
	filename = sys.argv[1]	
	replace(filename,"INSERT_ORDERER_CA_CERT",orderer_cert)

	replace(filename,"INSERT_ORG1_CA_CERT",org1_cert)

	replace(filename,"INSERT_ORG2_CA_CERT",org2_cert)
	
	print ("DONE")
if __name__ =="__main__":
	main()
