cd ./fabric-samples/first-network/

./byfn.sh -m down 
export PATH="$HOME/.yarn/bin:$PATH"
composer card delete -c PeerAdmin@byfn-network-org1
composer card delete -c PeerAdmin@byfn-network-org2
composer card delete -c alice@insurance_chain
composer card delete -c bob@insurance_chain
composer card delete -c admin@insurance_chain
composer card delete -c PeerAdmin@insurance_chain

rm -fr $HOME/.composer

cd ../../
rm -rf ./fabric-samples
rm -rf ./composer
