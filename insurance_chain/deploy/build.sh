export PATH="$HOME/.yarn/bin:$PATH"


git clone https://github.com/mahoney1/fabric-samples.git
cd fabric-samples
curl -sSL http://bit.ly/2ysbOFE | bash -s 1.2.0 1.2.0 0.4.10
git checkout multi-org

cd first-network

./byfn.sh -m generate
./byfn.sh -m up -s couchdb -a

cd ../../

mkdir -p ./composer/org1
mkdir -p ./composer/org2


echo "Building ORG1 byfn JSON"

cp -r ./byfn-network.json ./composer/org1/byfn-network-org1.json

sed -i 's/$ORG/Org1/g' ./composer/org1/byfn-network-org1.json

python ./replace.py ./composer/org1/byfn-network-org1.json

export ORG1=./fabric-samples/first-network/crypto-config/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp

cp -p $ORG1/signcerts/A*.pem ./composer/org1

cp -p $ORG1/keystore/*_sk ./composer/org1

echo "DONE BUILDING ORG1"

echo "Build ORG2 byfn JSON"

cp -r ./byfn-network.json ./composer/org2/byfn-network-org2.json

sed -i 's/$ORG/Org2/g' ./composer/org2/byfn-network-org2.json

python ./replace.py ./composer/org2/byfn-network-org2.json

export ORG2=./fabric-samples/first-network/crypto-config/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp

cp -p $ORG2/signcerts/A*.pem ./composer/org2

cp -p $ORG2/keystore/*_sk ./composer/org2
echo "DONE BUILDING ORG2"

cd ./composer

composer card create -p org1/byfn-network-org1.json -u PeerAdmin -c org1/Admin@org1.example.com-cert.pem -k org1/*_sk -r PeerAdmin -r ChannelAdmin -f PeerAdmin@byfn-network-org1.card

composer card create -p org2/byfn-network-org2.json -u PeerAdmin -c org2/Admin@org2.example.com-cert.pem -k org2/*_sk -r PeerAdmin -r ChannelAdmin -f PeerAdmin@byfn-network-org2.card

composer card import -f PeerAdmin@byfn-network-org1.card --card PeerAdmin@byfn-network-org1

composer card import -f PeerAdmin@byfn-network-org2.card --card PeerAdmin@byfn-network-org2

composer network install --card PeerAdmin@byfn-network-org1 --archiveFile ../../dist/insurance_chain.bna

composer network install --card PeerAdmin@byfn-network-org2 --archiveFile ../../dist/insurance_chain.bna

composer identity request -c PeerAdmin@byfn-network-org1 -u admin -s adminpw -d alice

composer identity request -c PeerAdmin@byfn-network-org2 -u admin -s adminpw -d bob


composer network start -c PeerAdmin@byfn-network-org1 -n insurance_chain -V 0.0.3 -o endorsementPolicyFile=../endorsement-policy.json -A alice -C alice/admin-pub.pem -A bob -C bob/admin-pub.pem

composer card create -p org1/byfn-network-org1.json -u alice -n insurance_chain -c alice/admin-pub.pem -k alice/admin-priv.pem

composer card import -f alice@insurance_chain.card


composer card create -p org2/byfn-network-org2.json -u bob -n insurance_chain -c bob/admin-pub.pem -k bob/admin-priv.pem

composer card import -f bob@insurance_chain.card
