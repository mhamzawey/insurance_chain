yarn  global add composer-cli
yarn  global add composer-rest-server
yarn  global add generator-hyperledger-composer
yarn  global add yo
yarn  global add composer-playground


mkdir ~/fabric-tools && cd ~/fabric-tools

curl -O https://raw.githubusercontent.com/hyperledger/composer-tools/master/packages/fabric-dev-servers/fabric-dev-servers.zip
unzip fabric-dev-servers.zip

export PATH="$HOME/.yarn/bin:$PATH"
